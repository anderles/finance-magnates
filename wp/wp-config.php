<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wpfm');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ND m91B3,1JAN}D!*,ID;#tLjGo!)X oBt%V#)z<DoI~{c;H-j4WJ/}&(AYLd _:');
define('SECURE_AUTH_KEY',  'FKESO~8-Sr_:5nq:`ZXOm!TR&8f;Bi=*ij]Fc{U##?:rlxV;k(%oRhP}$PC1K<->');
define('LOGGED_IN_KEY',    '^B2OL4BJ@cbG1xQS$=.(qIC]x Y1.SOU}oi,Z/WvGQ;Nl7wYW7HT<CV|5q.Sz(s5');
define('NONCE_KEY',        'qZX&kz>?j,zO>7QLkW)(q8!PH/O~QwE~R:|iJ:$ua-sq i4SYS.]14tzRr !Ar%N');
define('AUTH_SALT',        'Ol1?hrqnt@X~d7%a8S`s3%c^T].$377)EiJ[O*LgjSm0Q<5?L[IDOqk!px[MT]2d');
define('SECURE_AUTH_SALT', '$]oM+*NXy,gz9>T~K>K;k>i3W= }Sas*?FYaNW><T^:U@r@F(&C;wVXJI[[8%4;#');
define('LOGGED_IN_SALT',   '>Bt7%3478[DB>^<$aB]PYtDC=W)z>$z[xKf@wiWZ<u?cdxC$gDJ?AF=+:jZi%H q');
define('NONCE_SALT',       '8ynh w}!b:]qGEHS7XSL+<eTb;f!&05AWUVTB7`ug@iyxlG1!HKTV@W1Cq a>WH>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
