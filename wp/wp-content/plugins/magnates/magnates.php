<?php

/**
 * Plugin Name: MagnatesPostWidget
 * Plugin URI: http://finance-magnates.dev/
 * Description: N/A.
 * Version: 0.1.0
 * Author: Andrew Boyko
 * Author URI: N/A
 * License: GPL12
 */
class MagnatesPostWidget extends WP_Widget
{
    const POST_CATEGORY_ID = 2;
    const POSTS_PER_PAGE = 5;
    const ORDER_BY = 'rand';

    public function __construct()
    {
        $widget_ops = [
            'classname'   => 'MagnatesPostWidget',
            'description' => 'Displays a random post',
        ];
        $this->WP_Widget(
            'MagnatesPostWidget',
            'Magnates Post',
            $widget_ops
        );
    }

    public function form($instance)
    {
        $instance = wp_parse_args((array)$instance, ['title' => '']);
        $title    = $instance['title'];
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">
                Title: <input class="widefat"
                              id="<?php echo $this->get_field_id('title'); ?>"
                              name="<?php echo $this->get_field_name('title'); ?>"
                              type="text"
                              value="<?php echo attribute_escape($title); ?>"/>
            </label>
        </p>
        <?php
    }

    function update($new_instance, $old_instance)
    {
        $instance          = $old_instance;
        $instance['title'] = $new_instance['title'];

        return $instance;
    }

    public function widget($args, $instance)
    {
        extract($args, EXTR_SKIP);

        echo $before_widget;
        $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);

        if ( ! empty($title)) {
            echo $before_title . $title . $after_title;
        }

        query_posts(
            'cat=' . self::POST_CATEGORY_ID . '&posts_per_page=' . self::POSTS_PER_PAGE . '&orderby=' . self::ORDER_BY
        );
        if (have_posts()) :
            echo '<ul class="post_list">';
            while (have_posts()) : the_post();
                echo '<li><a href="' . get_the_permalink(get_the_ID(), false) . '">' . get_the_title() . '</a></li>';
            endwhile;
            echo '</ul>';
        endif;
        wp_reset_query();

        echo $after_widget;
    }
}