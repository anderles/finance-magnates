<?php
/**
 * The Template for displaying all single posts
 *
 * @package    WordPress
 * @subpackage Twenty_Twelve
 * @since      Twenty Twelve 1.0
 */

get_header(); ?>

    <div id="primary" class="site-content">
        <div id="content" role="main">

            <?php while (have_posts()) : the_post(); ?>

                <?php get_template_part('content', get_post_format()); ?>
                <?php
                $img = get_stylesheet_directory_uri() . '/img/bg-triangle.png';
                ?>
                <nav class="nav-single">
                    <?php if (get_preview_post_link()): ?>
                        <span class="nav-previous"><img src="<?php echo $img; ?>"/><?php previous_post_link('%link',
                                _x('Left ', 'Previous post link',
                                    'magnates') . '%title</span>'); ?></span>
                    <?php endif; ?>
                    <?php if (get_next_post_link()): ?>
                        <span class="nav-next"><img src="<?php echo $img; ?>"/><?php next_post_link('%link',
                                _x('Right ', 'Next post link',
                                    'magnates') . '%title</span>'); ?></span>
                    <?php endif; ?>
                </nav><!-- .nav-single -->

                <?php comments_template('', true); ?>

            <?php endwhile; // end of the loop. ?>

        </div><!-- #content -->
    </div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>