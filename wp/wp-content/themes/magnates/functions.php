<?php
/**
 * Magnates functions and definitions
 *
 * @package WordPress
 * @subpackage Magnates
 * @since Magnates 0.1.0
 */

function bootstrap_scripts() {
    wp_enqueue_style(
        'bootstrap_css',
        get_stylesheet_directory_uri() . '/assets/vendor/bootstrap/dist/css/bootstrap.css',
        false,
        null
    );
    wp_register_script(
        'bootstrap_js',
        get_stylesheet_directory_uri() . '/assets/vendor/bootstrap/dist/js/bootstrap.js',
        array('jquery'),
        null,
        true
    );
    wp_enqueue_script('bootstrap_js');
}

add_action('wp_enqueue_scripts', 'bootstrap_scripts');

function magnates_post_widget() {
    register_widget('MagnatesPostWidget');
}

add_action('widgets_init', 'magnates_post_widget');