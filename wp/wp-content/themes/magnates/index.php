<?php
/**
 * The main template file
 *
 * @package    WordPress
 * @subpackage Magnates
 * @since      Magnates 0.1.0
 */

get_header(); ?>

    <div id="primary" class="site-content">
        <div id="content" role="main">
            <?php if (have_posts()) : ?>

                <?php
                while (have_posts()) : the_post(); ?>
                    <?php get_template_part('content', 'page'); ?>
                    <?php comments_template('', true); ?>
                    <nav id="nav-single">
                        <?php posts_nav_link(''); ?>
                        <?php
                        $img = get_stylesheet_directory_uri() . '/img/bg-triangle.png';
                        ?>
                        <span class="nav-previous"><img src="<?php echo $img; ?>"/><?php previous_post_link('%link',
                                _x('Left ', 'Previous post link',
                                    'magnates') . '%title</span>'); ?></span>
                    </nav><!-- #nav-single -->
                    <?php break; ?>
                <?php endwhile; // end of the loop. ?>
                <?php
            // If no content, include the "No posts found" template.
            else :
                get_template_part('template-parts/content', 'none');
            endif;
            ?>
        </div><!-- #content -->
    </div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>